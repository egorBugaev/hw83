const express = require('express');
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const nanoid = require('nanoid');
const auth =require('../Middleware/auth');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const user = new User(req.body);

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error));
    });

    router.post('/sessions',auth, async (req, res) => {
	    let user = req.user;
	    if (!user) {
	     user = await User.findOne({username: req.body.username});
	    }
        if (!user) {
            return res.status(400).send({error: 'Username not found'});
        }

        const isMatch = await bcrypt.compare(req.body.password, user.password);

        if (!isMatch) {
            return res.status(400).send({error: 'Password is wrong'});
        }

	    user.token = nanoid();
	    user.save()
		    .then(user => res.send(user))
		    .catch(error => res.status(400).send(error));
    });



    return router;
};

module.exports = createRouter;
