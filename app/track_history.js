const express = require('express');
const Track_history = require('../models/Track_history');
const auth =require('../Middleware/auth');

const createRouter = () => {
	const router = express.Router();

	router.post('/', auth,(req, res) => {
		const track_history = new Track_history(req.body);
		track_history.date= new Date();
		track_history.user= req.user.username;
		track_history.save()
			.then(track_history => res.send(track_history))
			.catch(error => res.status(400).send(error));
	});

	return router;
};

module.exports = createRouter;
