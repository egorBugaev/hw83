const mongoose = require('mongoose');
const Schema =  mongoose.Schema;

const Track_historySchema = new Schema({
	user: {
		type: String,
		required: true,
	},
	track:{
		type: Schema.Types.ObjectId,
		required: true,
		ref: 'tracks'
	},
	date:{
		type:Date,
		required: true
	}
});

const Track_history = mongoose.model('Track_history', Track_historySchema);

module.exports = Track_history;